// работа модального окна
	var modal = document.querySelector(".write-us-modal");
	var buttonWrite = document.querySelector(".contacts > .btn");
	var closeModalBtn = document.querySelector(".write-us-modal > .close");
	var modalMap = document.querySelector(".modal-map");
	var buttonMap  = document.querySelector(".contacts > .contacts-map");
	var closeModalMapBtn = document.querySelector(".modal-map > .close");

	buttonWrite.addEventListener("click", function(event){
		event.preventDefault();
		modal.classList.add('write-us-modal-visible');
	});

	buttonMap.addEventListener("click", function(event){
		event.preventDefault();
		modalMap.classList.add('modal-map-visible');
	});

	closeModalBtn.addEventListener('click', function(){
		closeModal();
	});

	closeModalMapBtn.addEventListener('click', function(){
		closeModalMap();
	});

	window.addEventListener('keydown', function(event){
		if (event.key === 'Escape'){
			closeModal();
			closeModalMap();
		}
	});

// отправка формы
	var form = modal.querySelector('form');
	var inputs = [];
	inputs.push(form.querySelector('#name'));
	inputs.push(form.querySelector('#e-mail'));
	inputs.push(form.querySelector('#textarea'));

	inputs.forEach(function(item, index) {
		inputs[index].addEventListener('keydown', function() {
			var spanShow = item.parentNode.querySelector('span');
			removeError(spanShow, item);
		})
	})

	form.addEventListener('submit', function(event) {
		var hasErrors = false;
		inputs.forEach(function(item, index){
			var spanShow = item.parentNode.querySelector('span');
			if (item.value === '') {
				addError(spanShow, item);
				hasErrors = true;
			}
			else {
				removeError(spanShow, item);
			}
		});
		if (hasErrors) {
			event.preventDefault();
		} else {
			alert('Форма будет отправлена!');
		}
	})


//слайдер
var slider = document.querySelector('.slider');
var slides = [];
	slides.push(slider.querySelector('.hammers'));
	slides.push(slider.querySelector('.drills'));
var btnPrv = document.querySelector('.slider-buttons > .slider-button-left');
var btnNext = document.querySelector('.slider-buttons > .slider-button-right');
var points = [];
	points.push(slider.querySelector('.slider-points > .slider-point-hammers'));
	points.push(slider.querySelector('.slider-points > .slider-point-drills'));
var IndexNext = 0;
var IndexPrv = -1;

	btnNext.addEventListener('click', function() {
		SlideRemove();
		PointRemove();
		IndexNext++;
		if (IndexNext < slides.length) {
			slides[IndexNext].classList.add('slide-active');
			points[IndexNext].classList.add('active');
		}
		else {
			slides[0].classList.add('slide-active');
			points[0].classList.add('active');
			IndexNext = 0;
		}
	});

	btnPrv.addEventListener('click', function() {
		SlideRemove();
		PointRemove();
		IndexPrv++;
		if ((IndexPrv > 0) && (IndexPrv < slides.length)) {
			slides[IndexPrv-1].classList.add('slide-active');
			points[IndexPrv-1].classList.add('active');
		}
		else {
			slides[slides.length - 1].classList.add('slide-active');
			points[slides.length - 1].classList.add('active');
			IndexPrv = 0;
		}
	});

	points.forEach(function(item,indexPoint){
		points[indexPoint].addEventListener('click', function(){
			PointRemove();
			points[indexPoint].classList.add('active');
			slides.forEach(function(item,indexSlide){
				slides[indexSlide].classList.remove('slide-active');
				slides[indexPoint].classList.add('slide-active');
			})
		})
	})


//функции
	function closeModal(){
		modal.classList.remove('write-us-modal-visible');
	}

	function closeModalMap(){
		modalMap.classList.remove('modal-map-visible');
	}

	function removeError(span, item) {
		span.classList.remove('span-visible');
		item.classList.remove('input-error');
	}

	function addError(span, item) {
		span.classList.add('span-visible');
		item.classList.add('input-error');
	}

	function SlideRemove() {
		slides.forEach(function(item,index){
			slides[index].classList.remove('slide-active');
		})
	}

	function PointRemove() {
		points.forEach(function(item,index){
			points[index].classList.remove('active');
		})
	}