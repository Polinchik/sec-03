// работа модального окна
	var modal = document.querySelector(".write-us-modal");
	var buttonWrite = document.querySelector(".contacts > .btn");
	var closeModalBtn = document.querySelector(".write-us-modal > .close");
	var modalMap = document.querySelector(".modal-map");
	var buttonMap  = document.querySelector(".contacts > .map");
	var closeModalMapBtn = document.querySelector(".modal-map > .close");

	buttonWrite.addEventListener("click", function(event){
		event.preventDefault();
		modal.classList.add('write-us-modal-visible');
	});

	buttonMap.addEventListener("click", function(event){
		event.preventDefault();
		modalMap.classList.add('modal-map-visible');
	});

	closeModalBtn.addEventListener('click', function(){
		closeModal();
	});

	closeModalMapBtn.addEventListener('click', function(){
		closeModalMap();
	});

	window.addEventListener('keydown', function(event){
		if (event.key === 'Escape'){
			closeModal();
			closeModalMap();
		}
	});

	// отправка формы
	var form = modal.querySelector('form');
	var inputs = [];
	inputs.push(form.querySelector('#name'));
	inputs.push(form.querySelector('#e-mail'));
	inputs.push(form.querySelector('#textarea'));

	form.addEventListener('submit', function(event) {

		var hasErrors = false;

		inputs.forEach(function(item, index){
			var spanShow = item.parentNode.querySelector('span');
			if (item.value === '') {
				spanShow.classList.add('span-visible');
				item.classList.add('input-error');
				hasErrors = true;
			}
			else {
				spanShow.classList.remove('span-visible');
				item.classList.remove('input-error');
			}
		});

		if (hasErrors) {
			event.preventDefault();
		} else {
			alert('Форма будет отправлена!');
		}
	})


//слайдер
var slider = document.querySelector('.slider');
var slides = [];
	slides.push(slider.querySelector('.hammers'));
	slides.push(slider.querySelector('.drills'));
var btnPrv = document.querySelector('.slider-buttons > .slider-button-left');
var btnNext = document.querySelector('.slider-buttons > .slider-button-right');
var pointNext = document.querySelector('.slider-points > .slider-point-right');
var pointPrv = document.querySelector('.slider-points > .slider-point-left');

	btnNext.addEventListener('click', function() {
			for (i = 0; i < slides.length; i++) {
				slides[i].classList.remove('slide-active');
				slides[++i].classList.add('slide-active');
				pointPrv.classList.remove('active');
				pointNext.classList.add('active');
			}
	});

	btnPrv.addEventListener('click', function() {
			for (i = 1; i < slides.length; i++) {
				slides[i].classList.remove('slide-active');
				slides[i-1].classList.add('slide-active');
				pointNext.classList.remove('active');
				pointPrv.classList.add('active');
			}
	});

	pointNext.addEventListener('click', function() {
			for (i = 0; i < slides.length; i++) {
				slides[i].classList.remove('slide-active');
				slides[++i].classList.add('slide-active');
				pointPrv.classList.remove('active');
				pointNext.classList.add('active');
			}
	});

	pointPrv.addEventListener('click', function() {
			for (i = 1; i < slides.length; i++) {
				slides[i].classList.remove('slide-active');
				slides[i-1].classList.add('slide-active');
				pointNext.classList.remove('active');
				pointPrv.classList.add('active');
			}
	});






//функции

	function closeModal(){
			modal.classList.remove('write-us-modal-visible');		
	}

	function closeModalMap(){
			modalMap.classList.remove('modal-map-visible');		
	}

	function nextSlide(item) {
		
	}

	function prvSlide(item) {
		
	}